Application created for learning **Apache Flink** and connecting it to **Kafka** and **Redis**.

_DataProducer_ subscribes to HackerNews using PubNub and passes the news data into Kafka.
_DataConsumer_ reads these data from Kafka and filters the news that do not contain a word from the dictionary, deletes irrelevant information from it then writes it to Redis.

Go application will just read data from Redis and show it on the web interface.