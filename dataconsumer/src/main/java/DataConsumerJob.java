import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Properties;

public class DataConsumerJob {

    public static final String REDIS_HOST = "localhost";
    public static final String KAFKA_SERVERS = "localhost:9092";
    public static final String KAFKA_TOPIC = "gamedatain";

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", KAFKA_SERVERS);

        FlinkKafkaConsumer<String> kafkaSource = new FlinkKafkaConsumer<>(KAFKA_TOPIC,
                new ProducerStringDeserializationSchema(KAFKA_TOPIC),
                properties);

        // It will filter all the news that do not contain one of the words.
        final ArrayList<String> look = new ArrayList<>();
        look.add("data");
        look.add("flink");
        look.add("apache");
        look.add("hadoop");
        look.add("cloud");
        look.add("turkey");
        look.add("rust");

        DataStream<Tuple2<String, String>> stream_get = env.addSource(kafkaSource)
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return isJson(s);
                    }
                })
                .map(new MapFunction<String, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> map(String s) throws Exception {
                        JsonParser parser = new JsonParser();
                        JsonObject j = parser.parse(s).getAsJsonObject();
                        String title = j.get("title").getAsString();
                        String link = j.get("link").getAsString();
                        Tuple2<String, String> tup = new Tuple2<>(title, link);
                        return tup;
                    }
                })
                .filter(new FilterFunction<Tuple2<String, String>>() {
                    @Override
                    public boolean filter(Tuple2<String, String> s) throws Exception {
                        String[] words = s.f0.toLowerCase().split( "\\W+" );
                        for(String word : words) {
                            if(look.contains(word)) {
                                return true;
                            }
                        }
                        return false;
                    }
                })
                .keyBy(0)
                .flatMap(new RichFlatMapFunction<Tuple2<String, String>, Tuple2<String, String>>() {
                    private transient ValueState<Boolean> check;
                    @Override
                    public void flatMap(Tuple2<String, String> input, Collector<Tuple2<String, String>> collector) throws Exception {
                        Boolean ch = check.value();
                        if(!ch) {
                            collector.collect(input);
                            check.update(Boolean.TRUE);
                        }
                    }
                    @Override
                    public void open(Configuration parameters) throws Exception {
                        ValueStateDescriptor<Boolean> descriptor =
                                new ValueStateDescriptor<Boolean>("check",
                                        TypeInformation.of(new TypeHint<Boolean>() {
                                        }),
                                        Boolean.FALSE);
                        check = getRuntimeContext().getState(descriptor);
                    }
                });

        FlinkJedisPoolConfig redis_conf = new FlinkJedisPoolConfig.Builder().setHost(REDIS_HOST).build();

        stream_get.addSink(new RedisSink<Tuple2<String, String>>(redis_conf, new RedisHNewsMapper()));

        env.execute();
    }

    public static class RedisHNewsMapper implements RedisMapper<Tuple2<String, String>> {

        @Override
        public RedisCommandDescription getCommandDescription() {
            return new RedisCommandDescription(RedisCommand.HSET, "title:link");
        }

        @Override
        public String getKeyFromData(Tuple2<String, String> input) {
            return input.f0;
        }

        @Override
        public String getValueFromData(Tuple2<String, String> input) {
            return input.f1;
        }
    }

    public static boolean isJson(String Json) {
        Gson gson = new Gson();
        try {
            gson.fromJson(Json, Object.class);
            Object jsonObjType = gson.fromJson(Json, Object.class).getClass();
            if(jsonObjType.equals(String.class)){
                return false;
            }
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
