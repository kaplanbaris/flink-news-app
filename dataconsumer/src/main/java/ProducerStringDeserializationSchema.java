import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.nio.charset.StandardCharsets;

public class ProducerStringDeserializationSchema implements KafkaDeserializationSchema<String> {

    private String topic;

    public ProducerStringDeserializationSchema(String topic) {
        super();
        this.topic = topic;
    }

    @Override
    public boolean isEndOfStream(String s) {
        return false;
    }

    @Override
    public String deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
        String result = new String(consumerRecord.value(), StandardCharsets.UTF_8);
        return result;
    }

    @Override
    public TypeInformation<String> getProducedType() {
        return TypeInformation.of(String.class);
    }
}
