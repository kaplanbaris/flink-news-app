module client

go 1.15

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.0.0-beta.9 // indirect
	github.com/hoisie/web v0.1.0 // indirect
)
