package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"html/template"
	"net/http"
)

var (
	ctx = context.Background()

	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
)

// For some reason it does not work if it does not start with uppercase
type NewsData struct {
	Title string
	Link string
}


type PageData struct {
	Title string
	Data []NewsData
}

func index(w http.ResponseWriter, r *http.Request) {
	val, err := rdb.HGetAll(ctx, "title:link").Result();

	if err != nil {
		panic(err);
	}

	tmpl := template.Must(template.ParseFiles("static/index.html"))

	var data []NewsData

	for title, link := range val {
		d := NewsData{
			Title: title,
			Link: link,
		}
		data = append(data, d)
	}

	fmt.Println(data)

	tmpl.Execute(
		w,
		PageData{
		Title: "HackerNews",
		Data: data,
	},
	)
}

func main() {
	http.HandleFunc("/", index)

	http.ListenAndServe(":8000", nil)

}
