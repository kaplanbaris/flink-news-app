import com.google.gson.JsonElement;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.pubnub.api.models.consumer.pubsub.PNSignalResult;
import com.pubnub.api.models.consumer.pubsub.files.PNFileEventResult;
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNMembershipResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNSpaceResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNUserResult;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Arrays;
import java.util.Properties;

public class DataProducerJob {

    public static final String KAFKA_HOST = "localhost:9092";
    public static final String KAFKA_TOPIC = "gamedatain";

    public static final String PN_SUBKEY = "sub-c-c00db4fc-a1e7-11e6-8bfd-0619f8945a4f";

    public static void main(String[] args) throws Exception {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", KAFKA_HOST);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        final KafkaProducer<byte[], String> kp = new KafkaProducer<>(properties);

        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(PN_SUBKEY);

        PubNub pubnub = new PubNub(pnConfiguration);

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
            }

            @Override
            public void message(PubNub pubNub,  PNMessageResult pnMessageResult) {
                JsonElement messages = pnMessageResult.getMessage();
                for(JsonElement message : messages.getAsJsonArray()){
                    kp.send(new ProducerRecord<byte[], String>(KAFKA_TOPIC, message.toString()));
                }
            }

            @Override
            public void presence(PubNub pubNub,  PNPresenceEventResult pnPresenceEventResult) {

            }

            @Override
            public void signal(PubNub pubNub,  PNSignalResult pnSignalResult) {

            }

            @Override
            public void user(PubNub pubNub,  PNUserResult pnUserResult) {

            }

            @Override
            public void space(PubNub pubNub,  PNSpaceResult pnSpaceResult) {

            }

            @Override
            public void membership(PubNub pubNub,  PNMembershipResult pnMembershipResult) {

            }

            @Override
            public void messageAction(PubNub pubNub,  PNMessageActionResult pnMessageActionResult) {

            }

            @Override
            public void file( PubNub pubNub,  PNFileEventResult pnFileEventResult) {

            }
        });

        pubnub.subscribe()
                .channels(Arrays.asList("hacker-news"))
                .execute();
    }

}
